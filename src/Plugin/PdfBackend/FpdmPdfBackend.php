<?php

namespace Drupal\fillpdf_fpdm\Plugin\PdfBackend;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileInterface;
use Drupal\file\Entity\File;
use Drupal\fillpdf\FieldMapping\TextFieldMapping;
use Drupal\fillpdf\Plugin\PdfBackendBase;
use Drupal\fillpdf\FillPdfBackendPluginInterface;
use Drupal\fillpdf\FillPdfFormInterface;
use FPDM;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fpdm PdfBackend plugin.
 *
 * @PdfBackend(
 *   id = "fpdm",
 *   label = @Translation("Fpdm"),
 *   description = @Translation(
 *     "FPDM Class. See <a href=':url'>documentation</a>.",
 *     arguments = {
 *       ":url" = "https://github.com/codeshell/fpdm"
 *     }
 *   ),
 *   weight = 10
 * )
 */
class FpdmPdfBackend extends PdfBackendBase implements ContainerFactoryPluginInterface, FillPdfBackendPluginInterface {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Constructs a plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, FileSystem $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function parse(FillPdfFormInterface $fillpdf_form) {
    $template_file = File::load($fillpdf_form->file->target_id);
    return $this->parseFile($template_file);
  }

  /**
   * {@inheritdoc}
   */
  public function parseStream($pdf_content) {
    $template_file = file_save_data($pdf_content);
    return $this->parseFile($template_file);
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile(FileInterface $template_file) {
    $template_uri = $template_file->getFileUri();
    $template_path = $this->fileSystem->realpath($template_uri);
    $fpdm = new FPDM($template_path);
    $output = [];
    $fpdm->parsePDFEntries($output);

    // Unset the xref info.
    unset($output['$_XREF_$']);

    $fields = [];
    foreach (array_keys($output) as $name) {
      $field = [
        'name' => $name,
        'type' => 'default',
      ];
      $fields[] = $field;
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function populateWithFieldData(FillPdfFormInterface $fillpdf_form, array $field_mapping, array $context) {
    $template_file = File::load($fillpdf_form->file->target_id);
    return $this->mergeFile($template_file, $field_mapping, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function mergeStream($pdf_content, array $field_mappings, array $context) {
    $template_file = file_save_data($pdf_content);
    return $this->mergeFile($template_file, $field_mappings, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function mergeFile(FileInterface $template_file, array $field_mappings, array $context) {
    $template_uri = $template_file->getFileUri();
    $fields = [];
    foreach ($field_mappings as $pdf_key => $mapping) {
      if ($mapping instanceof TextFieldMapping) {
        $fields[$pdf_key] = (string) $mapping;
      }
    }

    $read_only = isset($context['read_only']) ? $context['read_only'] : FALSE;

    $template_path = $this->fileSystem->realpath($template_uri);
    $fpdm = new \FPDM($template_path);
    $fpdm->Load($fields, TRUE);
    $fpdm->Merge(FALSE, $read_only);
    $data = $fpdm->Output('S');

    if ($data) {
      return $data;
    }

    return NULL;
  }

}
